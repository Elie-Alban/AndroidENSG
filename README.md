# Cours d'Android


## Introduction

### Objectifs

* Bases de la programmation Android
* Découverte d'Android Studio
* Création de GUI
* Cartographie sur mobiles

### Plan du cours

[Plan du cours](plan_du_cours.pdf) (une reformulation de ce document avec des images mais pas de liens...)


## [Ressources](0_ressources)

Cette première partie regroupe tous les documents explicatifs : les présentations vues en cours et des documents à lire de votre côté qui détaillent un peu plus.

* [Présentation rapide d'Android](0_ressources/presentation_android.pdf) ;
* [Installation d'Android Studio](0_ressources/installation_android_studio.md) ;
* [Android Studio](0_ressources/android_studio.md).


## Une première application

Hello World!

[Énoncé du TD](1_hello_world/README.md)


## Une deuxième application

Pile ou Face

[Énoncé du TD](2_pile_ou_face/README.md)


## Google Services

Carte, géolocalisation et géo-codage avec les services Google.

[Énoncé du TD](3_google_services/README.md)


## Prise de photo

[Énoncé du TD](4_photo/README.md)


## Utilisation de SQLite

Une base de données pour les stocker toutes.

[Énoncé du TD](5_sqlite/README.md)


## Résumé

[Résumé](7_resume/README.md)
